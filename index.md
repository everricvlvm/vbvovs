## 1. Цель работы
Ознакомиться с архитектурой Vagrant, опубликовать образ Vagrant на Blade-сервер и соорудить кластер на его основе.
## 2. Ход выполнения

### 2.1 Подготовка рабочего окружения
Вначале сделаем удобный вход по ssh на Blade-сервер. Поставим автовход по rsa ключу и прицепим псевдоним на sshgup:
```bash
alias sshguap="ssh -XYC -p 6667 spo_01@openit.guap.ru"
alias sshvm="ssh -p 2210 rancher@127.0.0.1"
alias mon="vboxmanage startvm"
alias mof="vboxmanage controlvm"
alias mch="vboxmanage modifyvm"
```
Также было добалено для удобства ещё несколько псевдонимов.

### 2.2 Настройка виртуальной машины
Для создания образа Vagrant настроим виртуальную машину `rns` так:
  * 2048 Мб ОЗУ;
  * Создадим динамически расширяемый до 200 Гб виртуальный жесткий диск;
  * Отключим поддержку аудио;
  * Режим NAT для 1-ого сетевого адаптера ВМ;
  * Порт 2210 хост-машины перенаправим на порт 22 ВМ.

### 2.3 Подготовка образа Линукс
Было решено выстраивать бокс на основе RancherOS, который можно взять здесь [rancheros.iso](https://releases.rancher.com/os/v1.5.0/rancheros.iso).
### 2.4 Установка Линукс на жёсткий диск
Сначала присоединим скачанный iso образ к ВМ. Затем запустим ВМ. 
Загрузим публичный ключ в `.ssh/` (для удобства из-под root):
```bash
wget https://gitlab.com/everricvlvm/vbvovs/raw/master/cloud-config.yml -O c.yml
ros install -d /dev/sda -c c.yml
```
Содержание cloud-config.yml:
```yml
#cloud-config
hostname: rancher
rancher:
  console: centos
  service: kernel-headers-system-docker

ssh_authorized_keys:
  - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCj/mhsSw1/PUNc3cH78KbKZ3sqGC3XsscNTb2LjoemzYMEom9yZTuJePjCIm4BW/yxYHkSStGYdn+But2KN6T7ofMFxbXt+6E4R4J5tbGySQtYzJBczA5EH0GlqEjjE5GXs+6iwhWvsBgSKv6cZLmIIdSN9Dq9lOstseyfO8OMfazXgdf7GM0pfXveoHsU1lzA0pE0V0Y/eQ0GTzK6nxknpc92xg+7fRiqBNnK8dZcj3IL1omejiBDJ1jxBKulEs1WASBxdVMWOtoGT+qB6k+aDg+CBJto2euf7nF+jZALZlO3dosRMcCZfsZEx3Z0yhGWTS42o7/oGhv4hMTvTb21 spo_01@b1eth0.ntl.edu
```
### 2.5 Первичная настройка Линукс
Теперь можно перезагрузить ВМ с диска и зайти на неё через ssh:
```bash
reboot
ssh -p 2210 rancher@127.0.0.1
```
Сделать консоль RancherOS, сохраняющей изменения при перезагрузке:
```bash
sudo ros console enable centos
sudo reboot
```
Добавим слой ядра, чтобы можно было установить VirtualBox-guest-additionals
```bash
sudo ros service enable kernel-headers-system-docker
sudo ros service up kernel-headers-system-docker
sudo yum update -y
sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum -y install gcc kernel-devel kernel-headers dkms make bzip2 perl
sudo reboot
```
Ставим гостевое дополнение (подключив его образ к ВМ):
```bash
sudo mkdir -p /media/vbg/
sudo mount /dev/cdrom /media/vbg
sudo cd /media/vbg
sudo ./VBoxLinuxAdditions.run
sudo reboot
```
Устанавливаем root-пароль 'vagrant' и подготавливаем к сжатию:
```bash
sudo passwd root
sudo dd if=/dev/zero of=/EMPTY bs=1M
sudo rm -f /EMPTY
```
Настроим автодоступ vagrant к аккаунту `rancher`:
```bash
sudo curl https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub >> /home/rancher/.ssh/authorized_keys
```

### 2.6 Настроим vagrant
Создадим папку `~/ovs` на Blade:
```bash
mkdir ~/ovs
cd ~/ovs
```
Создадим временный Vagrantfile:
```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  config.ssh.username = "rancher"
  config.vm.box = "ovs.box"
end
```
Построим `ovs.box` и поднимем его:
```bash
vagrant package -b rns -o ovs.box
vagrant up
```
=>
```
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'ovs.box' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: >= 0
==> default: Box file was not detected as metadata. Adding it directly...
==> default: Adding box 'ovs.box' (v0) for provider: virtualbox
    default: Unpacking necessary files from: file:///home/spo_01/ovs/ovs.box
==> default: Successfully added box 'ovs.box' (v0) for 'virtualbox'!
==> default: Importing base box 'ovs.box'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: ovs_default_1547133487455_47412
==> default: Fixed port collision for 22 => 2222. Now on port 2207.
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2207 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2207
    default: SSH username: rancher
    default: SSH auth method: private key
    default: 
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default: 
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
==> default: Mounting shared folders...
    default: /vagrant => /home/spo_01/ovs

```
### 2.7 Проверяем контейнер
```bash
vagrant ssh
```
=>
```
[spo_01@b1eth0 ovs]$ vagrant ssh
Last login: Thu Jan 10 14:58:19 2019 from 10.0.2.2
[rancher@rancher ~]$

```
### 2.8 Делаем кластер
`Vagrantfile`:
```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

$script = <<SCRIPT
sudo yum -y install nano mc wget
SCRIPT

Vagrant.configure(2) do |config|
  config.ssh.username = "rancher"
  config.vm.box = "ovs.box"

  config.vm.provision "shell", inline: $script

  # Master1
  config.vm.define :master1 do |m1|
    m1.vm.hostname = "master1.mycluster"
    m1.vm.network :private_network, ip: "192.168.0.12"
    m1.vm.provider :virtualbox do |vb|
      vb.memory = "2048"
    end
  end

  # Slave1
  config.vm.define :slave1 do |s1|
    s1.vm.hostname = "slave1.mycluster"
    s1.vm.network :private_network, ip: "192.168.0.21"
    s1.vm.provider :virtualbox do |vb|
      vb.memory = "2048"
    end
  end

  # Slave2
  config.vm.define :slave2 do |s2|
    s2.vm.hostname = "slave2.mycluster"
    s2.vm.network :private_network, ip: "192.168.0.22"
    s2.vm.provider :virtualbox do |vb|
      vb.memory = "2048"
    end
  end
end
```
=>
```
    ...
    master1: Complete!
    ...
    slave1: Complete!
    ...
    slave2: Complete!
```


Проверяем произвольный контейнер:
```bash
vagrant ssh slave2
```
=>
```
[spo_01@b1eth0 ovs]$ vagrant ssh slave2
Last login: Thu Jan 10 14:58:19 2019 from 10.0.2.2
[rancher@rancher ~]$ 
```
Итог:
```bash
vagrant global-status
```
=>
```bash
[spo_01@b1eth0 ovs]$ vagrant global-status
id       name    provider   state   directory                           
------------------------------------------------------------------------
0bcaa80  master1 virtualbox running /home/spo_01/ovs                   
5ddf1ea  slave1  virtualbox running /home/spo_01/ovs                   
14afe28  slave2  virtualbox running /home/spo_01/ovs  
```
## 3. Выводы
Произведено ознакомление с Vagrant и RancherOS. Удалённо создан кластер из нескольких контейнеров на основе личносозданного бокса.